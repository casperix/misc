# misc

Stupid set of auxiliary functions and types that are not included in specialized libraries

## deploy

### Local
Just add water:
- `./gradlew publishToMavenLocal`

### Nexus
Need variables (ask the administrator): 
`sonatypeStagingProfileId`,
`signing.keyId`,
`signing.password`,
`signing.secretKeyRingFile`.

Publishing, closing, releasing:
- `./gradlew publishToSonatype`
- `./gradlew findSonatypeStagingRepository closeAndReleaseSonatypeStagingRepository`

In case of troubles, see also:
- https://s01.oss.sonatype.org/#stagingRepositories
- https://github.com/gradle-nexus/publish-plugin