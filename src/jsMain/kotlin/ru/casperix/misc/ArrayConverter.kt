package ru.casperix.misc

import org.khronos.webgl.*

typealias Float32List = Float32Array
typealias Int32List = Int32Array
typealias Uint32List = Uint32Array

fun ByteArray.toJsInt8Array(): Int8Array {
    return Int8Array(toTypedArray())
}

fun ByteArray.toJsUInt8Array(): Uint8Array {
    return Uint8Array(toTypedArray())
}

fun ShortArray.toJsInt16Array(): Int16Array {
    return Int16Array(toTypedArray())
}

fun ShortArray.toJsUInt16Array(): Uint16Array {
    return Uint16Array(toTypedArray())
}

fun IntArray.toJsInt32Array(): Int32Array {
    return Int32Array(toTypedArray())
}

fun IntArray.toJsUint32Array(): Uint32Array {
    return Uint32Array(toTypedArray())
}

//fun LongArray.toJsBigInt64Array(): BigInt64Array {
//    return BigInt64Array(Array(size) {
//        this[it].toJsBigInt()
//    })
//}

fun FloatArray.toJsFloat32Array(): Float32Array {
    return Float32Array(toTypedArray())
}

fun DoubleArray.toJsFloat64Array(): Float64Array {
    return Float64Array(toTypedArray())
}


fun IntArray.toUint32List(): Uint32List {
    return Uint32List(toTypedArray())
}

fun IntArray.toInt32List(): Int32List {
    return Int32List(toTypedArray())
}

fun FloatArray.toFloat32List(): Float32List {
    return Float32List(toTypedArray())
}
