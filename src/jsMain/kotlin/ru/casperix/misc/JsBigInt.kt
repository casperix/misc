package ru.casperix.misc

@JsName("BigInt")
external class JsBigInt internal constructor()

public fun JsBigInt.toLong(): Long = toLong(this)

public fun Long.toJsBigInt(): JsBigInt = toJsBigInt(this)

private fun toLong(value: JsBigInt): Long {
    return js("value") as Long
}
private fun toJsBigInt(value: Long): JsBigInt = js("BigInt(value)")
