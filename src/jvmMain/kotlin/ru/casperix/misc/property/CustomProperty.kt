package ru.casperix.misc.property

interface CustomProperty<Value> {
    val name: String
    val setter: (Value) -> Unit
    val getter: () -> Value
}


class IntProperty(override val name: String, override val setter: (Int) -> Unit, override val getter: () -> Int) :
    CustomProperty<Int>

class StringProperty(
    override val name: String,
    override val setter: (String) -> Unit,
    override val getter: () -> String
) :
    CustomProperty<String>

class BooleanProperty(
    override val name: String,
    override val setter: (Boolean) -> Unit,
    override val getter: () -> Boolean
) :
    CustomProperty<Boolean>