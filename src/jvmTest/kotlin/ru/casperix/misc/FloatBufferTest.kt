package ru.casperix.misc

import ru.casperix.misc.DirectFloatBuffer
import ru.casperix.misc.clone
import ru.casperix.misc.floatBufferOf
import ru.casperix.misc.toDirectDoubleBuffer
import kotlin.test.Test
import kotlin.test.assertEquals

class FloatBufferTest {

	@Test
	fun testCreate() {
		DirectFloatBuffer(3)
	}

	@Test
	fun testCreateByFunction() {
		val buffer = DirectFloatBuffer(3) { it.toFloat() }
		assertEquals( 0f, buffer.get(0))
		assertEquals( 1.0f, buffer.get(1))
		assertEquals( 2.0f, buffer.get(2))
		assertEquals(3,  buffer.capacity())

	}

	@Test
	fun testCreateDirect() {
		val buffer = floatBufferOf(0f, 1f, 2f)
		assertEquals( 0f, buffer.get(0))
		assertEquals( 1f, buffer.get(1))
		assertEquals( 2f, buffer.get(2))
		assertEquals(3,  buffer.capacity())
	}

	@Test
	fun testClone() {
		val buffer = floatBufferOf(0f, 1f, 2f)
		val buffer2 = buffer.clone()

		buffer.put(2, 0f)
		assertEquals( 0f, buffer.get(0))
		assertEquals( 1f, buffer.get(1))
		assertEquals( 0f, buffer.get(2))

		assertEquals( 0f, buffer2.get(0))
		assertEquals( 1f, buffer2.get(1))
		assertEquals( 2f, buffer2.get(2))
	}

	@Test
	fun mapToDouble() {
		val buffer = floatBufferOf(0f, 1f, 2f)
		val buffer2 = buffer.toDirectDoubleBuffer()

		assertEquals( 0.0, buffer2.get(0))
		assertEquals( 1.0, buffer2.get(1))
		assertEquals( 2.0, buffer2.get(2))
		assertEquals(3,  buffer2.capacity())
	}
}