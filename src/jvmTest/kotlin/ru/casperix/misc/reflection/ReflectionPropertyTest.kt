package ru.casperix.misc.reflection

import ru.casperix.misc.property.IntProperty
import ru.casperix.misc.property.StringProperty
import kotlin.test.Test
import kotlin.test.assertEquals

class ReflectionPropertyTest {
    data class Settings(var some: String, var other: Int) {
        override fun toString(): String {
            return "Settings(some='$some', other=$other)"
        }
    }


    @Test
    fun test() {
        val initial = Settings("test", 23)
        val properties = ReflectionUtil.properties(initial)

        val expected = Settings("test works", 42)
        properties.forEach {
            if (it is StringProperty) {
                it.setter("test works")
            } else if (it is IntProperty) {
                it.setter(42)
            }
        }

        assertEquals(expected, initial)
    }
}