package ru.casperix.misc

import ru.casperix.misc.DirectDoubleBuffer
import ru.casperix.misc.clone
import ru.casperix.misc.doubleBufferOf
import ru.casperix.misc.toDirectFloatBuffer
import kotlin.test.Test
import kotlin.test.assertEquals

class DoubleBufferTest {

	@Test
	fun testCreate() {
		DirectDoubleBuffer(3)
	}

	@Test
	fun testCreateByFunction() {
		val buffer = DirectDoubleBuffer(3) { it.toDouble() }
		assertEquals( 0.0, buffer.get(0))
		assertEquals( 1.0, buffer.get(1))
		assertEquals( 2.0, buffer.get(2))
		assertEquals(3,  buffer.capacity())

	}

	@Test
	fun testCreateDirect() {
		val buffer = doubleBufferOf(0.0, 1.0, 2.0)
		assertEquals( 0.0, buffer.get(0))
		assertEquals( 1.0, buffer.get(1))
		assertEquals( 2.0, buffer.get(2))
		assertEquals(3,  buffer.capacity())
	}

	@Test
	fun testClone() {
		val buffer = doubleBufferOf(0.0, 1.0, 2.0)
		val buffer2 = buffer.clone()

		buffer.put(2, 0.0)
		assertEquals( 0.0, buffer.get(0))
		assertEquals( 1.0, buffer.get(1))
		assertEquals( 0.0, buffer.get(2))

		assertEquals( 0.0, buffer2.get(0))
		assertEquals( 1.0, buffer2.get(1))
		assertEquals( 2.0, buffer2.get(2))
	}

	@Test
	fun mapToDouble() {
		val buffer = doubleBufferOf(0.0, 1.0, 2.0)
		val buffer2 = buffer.toDirectFloatBuffer()

		assertEquals( 0f, buffer2.get(0))
		assertEquals( 1f, buffer2.get(1))
		assertEquals( 2f, buffer2.get(2))
		assertEquals(3,  buffer2.capacity())
	}
}