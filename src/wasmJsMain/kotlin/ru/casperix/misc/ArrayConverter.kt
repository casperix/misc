package ru.casperix.misc

import org.khronos.webgl.*

typealias Float32List = Float32Array
typealias Int32List = Int32Array
typealias Uint32List = Uint32Array

fun ByteArray.toJsArray(): JsArray<JsNumber> {
    return JsArray<JsNumber>().apply {
        forEachIndexed { index, item ->
            this[index] = item.toDouble().toJsNumber()
        }
    }
}

fun ShortArray.toJsArray(): JsArray<JsNumber> {
    return JsArray<JsNumber>().apply {
        forEachIndexed { index, item ->
            this[index] = item.toDouble().toJsNumber()
        }
    }
}

fun IntArray.toJsArray(): JsArray<JsNumber> {
    return JsArray<JsNumber>().apply {
        forEachIndexed { index, item ->
            this[index] = item.toDouble().toJsNumber()
        }
    }
}

fun LongArray.toJsArray(): JsArray<JsBigInt> {
    return JsArray<JsBigInt>().apply {
        forEachIndexed { index, item ->
            this[index] = item.toJsBigInt()
        }
    }
}

fun FloatArray.toJsArray(): JsArray<JsNumber> {
    return JsArray<JsNumber>().apply {
        forEachIndexed { index, item ->
            this[index] = item.toDouble().toJsNumber()
        }
    }
}


fun DoubleArray.toJsArray(): JsArray<JsNumber> {
    return JsArray<JsNumber>().apply {
        forEachIndexed { index, item ->
            this[index] = item.toJsNumber()
        }
    }
}

fun ByteArray.toJsInt8Array(): Int8Array {
    return Int8Array(toJsArray())
}

fun ByteArray.toJsUInt8Array(): Uint8Array {
    return Uint8Array(toJsArray())
}

fun ShortArray.toJsInt16Array(): Int16Array {
    return Int16Array(toJsArray())
}

fun ShortArray.toJsUInt16Array(): Uint16Array {
    return Uint16Array(toJsArray())
}


fun IntArray.toJsInt32Array(): Int32Array {
    return Int32Array(toJsArray())
}

fun IntArray.toJsUint32Array(): Uint32Array {
    return Uint32Array(toJsArray())
}

fun LongArray.toJsBigInt64Array(): BigInt64Array {
    return BigInt64Array(toJsArray())
}

fun FloatArray.toJsFloat32Array(): Float32Array {
    return Float32Array(toJsArray())
}

fun DoubleArray.toJsFloat64Array(): Float64Array {
    return Float64Array(toJsArray())
}


fun IntArray.toUint32List(): Uint32List {
    return Uint32List(toJsArray())
}

fun IntArray.toInt32List(): Int32List {
    return Int32List(toJsArray())
}

fun FloatArray.toFloat32List(): Float32List {
    return Float32List(toJsArray())
}
