package ru.casperix.misc


fun parseStringInArguments(source:Array<String>, name:String, default:String):String {
	val prefix = "$name="
	source.forEach { item->
		if (item.startsWith(prefix)) {
			return item.removePrefix(prefix)
		}
	}
	return default
}

fun parseIntInArguments(source:Array<String>, name:String, default:Int):Int {
	val raw = parseStringInArguments(source, name, default.toString())
	return raw.toIntOrNull() ?: default
}


