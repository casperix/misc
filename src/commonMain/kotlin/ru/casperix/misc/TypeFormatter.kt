package ru.casperix.misc

import kotlin.math.abs
import kotlin.math.roundToLong

fun Float.toPrecision(precision: Int): String {
	val precisionFactor = 10.pow(precision.clamp(0, 31))
	val normal = this * precisionFactor
	if (normal < Long.MIN_VALUE || normal > Long.MAX_VALUE || !normal.isFinite()) return normal.toString()

	val value = normal.roundToLong()
	val absValue = abs(value)
	val sign = if (value < 0) "-" else ""
	if (precision == 0) {
		return "$sign${absValue / precisionFactor}"
	}
	val remain = (absValue % precisionFactor).toString(precision)
	return "$sign${absValue / precisionFactor}.$remain"
}

fun Double.toPrecision(precision: Int): String {
	val precisionFactor = 10.pow(precision.clamp(0, 31))
	val normal = this * precisionFactor
	if (normal < Long.MIN_VALUE || normal == Double.NEGATIVE_INFINITY) return "-∞"
	if (normal > Long.MAX_VALUE || normal == Double.POSITIVE_INFINITY) return "+∞"
	if (normal.isNaN()) return "∞"

	val value = normal.roundToLong()
	val absValue = abs(value)
	val sign = if (value < 0) "-" else ""
	if (precision == 0) {
		return "$sign${absValue / precisionFactor}"
	}
	val remain = (absValue % precisionFactor).toString(precision)
	return "$sign${absValue / precisionFactor}.$remain"
}

fun Long.toString(minSymbolAmount: Int, prefix: Char = '0'): String {
	return this.toString().fillSpace(minSymbolAmount, prefix)
}

fun Int.toString(minSymbolAmount: Int, prefix: Char = '0'): String {
	return this.toString().fillSpace(minSymbolAmount, prefix)
}

fun String.fillSpace(minSymbolAmount: Int, prefix: Char): String {
	var buffer = this
	if (buffer.length > minSymbolAmount) {
		return buffer//.slice((buffer.length - size) until buffer.length)
	}
	while (buffer.length < minSymbolAmount) {
		buffer = "$prefix$buffer"
	}
	return buffer
}