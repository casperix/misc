package ru.casperix.misc.format

import ru.casperix.misc.clamp
import ru.casperix.misc.pow
import ru.casperix.misc.toString
import kotlin.math.abs
import kotlin.math.roundToInt
import kotlin.math.roundToLong

object StringUtil {
    val NON_BREAK_SPACE = Char(160).toString()
    val NUMBER_SEPARATOR = "'"
    val NUMBER_GROUP_SIZE = 3

    fun camelCaseSplit(source: String): List<String> {
        val camelCase = Regex("([A-Z][a-z0-9]*)")
        return camelCase.findAll(source).toList().map { it.value }
    }


    fun Long.formatGrouped(): String {
        return toString().reversed().chunked(NUMBER_GROUP_SIZE).reversed().joinToString(NUMBER_SEPARATOR) { it.reversed() }
    }

    fun Double.toPrecisionGrouped(precision: Int): String {
        val precisionFactor = 10.pow(precision.clamp(0, 31))
        val normal = this * precisionFactor
        if (normal < Long.MIN_VALUE || normal == Double.NEGATIVE_INFINITY) return "-∞"
        if (normal > Long.MAX_VALUE || normal == Double.POSITIVE_INFINITY) return "+∞"
        if (normal.isNaN()) return "∞"

        val value = normal.roundToLong()
        val absValue = abs(value)
        val sign = if (value < 0) "-" else ""
        val ceil = (absValue / precisionFactor).formatGrouped()
        if (precision == 0) {
            return "$sign$ceil"
        }
        val remain = (absValue % precisionFactor).toString(precision)
        return "$sign$ceil.$remain"
    }

    fun Float.formatAsPercent(): String {
        return (this * 100f).roundToInt().toString() + "%"
    }

}