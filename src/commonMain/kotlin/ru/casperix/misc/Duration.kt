package ru.casperix.misc

import kotlin.time.Duration
import kotlin.time.DurationUnit


fun Duration.toFloat(unit: DurationUnit): Float {
    return toDouble(unit).toFloat()
}