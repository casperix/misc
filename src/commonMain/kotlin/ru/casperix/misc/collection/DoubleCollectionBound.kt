package ru.casperix.misc.collection

import kotlin.math.max
import kotlin.math.min


data class DoubleCollectionBound(val middle: Double, val minValue: Double, val maxValue: Double) {
	companion object {
		fun calculate(values: Iterable<Double>): DoubleCollectionBound {
			var middle = 0.0
			var minValue = Double.POSITIVE_INFINITY
			var maxValue = Double.NEGATIVE_INFINITY
			var counter = 0
			values.forEach {
				counter++
				val value = it
				middle += value
				maxValue = max(maxValue, value)
				minValue = min(minValue, value)
			}
			middle /= counter.toDouble()
			return DoubleCollectionBound(middle, minValue, maxValue)
		}
	}
}