package ru.casperix.misc.collection


/**
 * 	Limited collection,
 * 	for last 1000 messages for example.
 */
class LimitedCollection<T>(val maxSize:Int, private val elements:MutableList<T>, val overheadBehaviour: Behaviour) : MutableList<T> by elements {
	enum class Behaviour {
		REMOVE_FIRST,
		REMOVE_LAST,
		IGNORE_NEW,
	}

	override fun add(element: T): Boolean {
		if (elements.size == maxSize) {
			when(overheadBehaviour) {
				Behaviour.REMOVE_FIRST ->elements.removeAt(0)
				Behaviour.REMOVE_LAST ->elements.removeAt(elements.size - 1)
				Behaviour.IGNORE_NEW ->return false
			}
		}
		return elements.add(element)
	}

	companion object {
		fun <T> stream(size:Int): LimitedCollection<T> {
			return LimitedCollection(size, mutableListOf(), Behaviour.REMOVE_FIRST)
		}
	}
}