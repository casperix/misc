package ru.casperix.misc.time

import kotlin.time.Duration
import kotlin.time.measureTime
import kotlin.time.measureTimedValue

expect fun setInterval(handler: () -> Unit, interval: Long)

@Deprecated(message = "use getTime")
expect fun getTimeMs():Long
expect fun getTime():Duration

/**
 *	execute a function and measure time
 *	It is deprecated and not precision method.
 *	Recommend use next variant:
 * @see measureTimedValue
 *
 * @param handler
 */
@Deprecated(message = "use measureTimedValue")
fun <Result> executeAndMeasure(handler: () -> Result): Pair<Long, Result> {
	val startTimeMs = getTimeMs()
	val result = handler()
	val finishTimeMs = getTimeMs()
	return Pair(finishTimeMs - startTimeMs, result)
}

/**
 *	It is deprecated and not precision method.
 *	Recommend use next variant:
 *@see measureTime
 **/
@Deprecated(message = "use measureTime")
fun measure(handler: () -> Unit): Long {
	val startTimeMs = getTimeMs()
	handler()
	val finishTimeMs = getTimeMs()
	return (finishTimeMs - startTimeMs)
}
