package ru.casperix.misc

enum class Tee {
	POSITIVE,
	ZERO,
	NEGATIVE,
}