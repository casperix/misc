package ru.casperix.misc

@Deprecated(message = "Extremely abstract")
interface Updatable {
    fun update()
}
