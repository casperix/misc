package ru.casperix.misc.executor.impl

import ru.casperix.misc.executor.Task
import ru.casperix.misc.executor.TaskState
import kotlin.concurrent.Volatile

open class WriteableTask<Result : Any> : Task<Result> {
    @Volatile
    var taskState: TaskState = TaskState.INITIALIZING

    @Volatile
    var result: Result? = null

    override fun getState(): TaskState {
        return taskState
    }

    override fun getResultOrNull(): Result? {
        return result
    }

    override fun waitResult(): Result {
        return getResultOrNull() ?: throw Exception("Invalid result")
    }
}