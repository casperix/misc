package ru.casperix.misc.executor

enum class TaskState {
    INITIALIZING,
    RUNNING,
    SUCCESS,
    FAIL,
}

interface Task<Output : Any> {
    fun getState(): TaskState
    fun getResultOrNull(): Output?
    fun waitResult():Output
}

interface ExecutorService {
    fun <Result : Any> execute(operation: () -> Result): Task<Result>
    fun close()
}

expect fun createExecutorService(threads: Int? = null): ExecutorService

