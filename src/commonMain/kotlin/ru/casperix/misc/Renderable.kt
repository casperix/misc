package ru.casperix.misc

@Deprecated(message = "Extremely abstract")
interface Renderable {
    fun render()
}