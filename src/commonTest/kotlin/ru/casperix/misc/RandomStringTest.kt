package ru.casperix.misc

import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class RandomStringTest {
	@Test
	fun randomString() {
		val random = Random(1)

		val amount = 10000
		val unique = mutableSetOf<String>()

		repeat(amount) {
			val s0 = random.nextString(0..0)
            assertEquals(0, s0.length)

			val s01 = random.nextString(0..1)
			assertTrue(s01.length in 0..1)

			val sCustom= random.nextString(10..100)
			assertTrue(sCustom.length in 10..100)

			unique += sCustom
		}

		assertEquals(unique.size, amount, "String repeats")
	}
}