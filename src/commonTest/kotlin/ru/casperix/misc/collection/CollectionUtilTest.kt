package ru.casperix.misc.collection

import ru.casperix.misc.sliceSafe
import ru.casperix.misc.splitByChunk
import kotlin.test.Test
import kotlin.test.assertEquals


class CollectionUtilTest {


	@Test
	fun splitTest() {
		val group = listOf(1, 2, 3, 4, 5)

		assertEquals(listOf(listOf(1), listOf(2), listOf(3), listOf(4), listOf(5)), group.splitByChunk(1), "invalid split")
		assertEquals(listOf(listOf(1, 2), listOf(3, 4), listOf(5)), group.splitByChunk(2), "invalid split")
		assertEquals(listOf(listOf(1, 2, 3), listOf(4, 5)), group.splitByChunk(3), "invalid split")
		assertEquals(listOf(listOf(1, 2, 3, 4), listOf(5)), group.splitByChunk(4), "invalid split")
		assertEquals(listOf(listOf(1, 2, 3, 4, 5)), group.splitByChunk(5), "invalid split")
		assertEquals(listOf(listOf(1, 2, 3, 4, 5)), group.splitByChunk(6), "invalid split")
		assertEquals(listOf(listOf(1, 2, 3, 4, 5)), group.splitByChunk(20), "invalid split")
	}

	@Test
	fun sliceTest() {
		val group = listOf(0,1, 2, 3, 4)

		assertEquals(listOf(2, 3), group.sliceSafe(2..3))
		assertEquals(listOf(3, 4), group.sliceSafe(3..4))

		assertEquals(listOf(0, 1, 2, 3, 4), group.sliceSafe(0..4))
		assertEquals(listOf(0, 1, 2, 3, 4), group.sliceSafe(0..5))
		assertEquals(listOf(0, 1, 2, 3, 4), group.sliceSafe(-1..4))
		assertEquals(listOf(0, 1, 2, 3, 4), group.sliceSafe(-1..5))

		assertEquals(listOf(1, 2, 3, 4), group.sliceSafe(1..5))

		assertEquals(listOf(), group.sliceSafe(-2..-1))
		assertEquals(listOf(), group.sliceSafe(5..6))

	}
}