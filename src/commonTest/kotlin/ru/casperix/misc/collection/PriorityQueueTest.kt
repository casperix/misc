package ru.casperix.misc.collection

import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertTrue

class PriorityQueueTest {
	private val numbers = mutableSetOf<Int>()
	private val randomNumbers: List<Int>
	private val randomNumbersSliced: List<Int>
	private val sortNumbers: List<Int>
	private val MAX = 1000

	init {
		val randomNumbersSliced = mutableListOf<Int>()
		val randomNumbers = mutableListOf<Int>()
		val random = Random(0)
		for (i in 1..MAX) {

			var value: Int
			do {
				value = random.nextInt()
			} while (numbers.contains(value))
			numbers.add(value)

			randomNumbers.add(value)
			if (random.nextDouble() > 0.9) {
				randomNumbersSliced.add(value)
			}
		}
		this.randomNumbersSliced = randomNumbersSliced
		this.randomNumbers = randomNumbers
		sortNumbers = randomNumbers.sorted()
	}

	@Test
	fun randomListTest() {
		val pq = PriorityQueue.create<Int>()
		for (n in randomNumbers) {
			pq.add(n)
		}
		for (m in sortNumbers) {
			assertEquals(m, pq.poll())
		}
	}

	@Test
	fun removeOneTest() {
		val pq = PriorityQueue.create<Int>()
		for (n in randomNumbers) {
			pq.add(n)
		}

		val removedValue = randomNumbersSliced[0]
		pq.remove(randomNumbersSliced[0])

		val sortNumbersCustom = sortNumbers.slice(sortNumbers.indices).toMutableList()
		sortNumbersCustom.remove(removedValue)

		for (m in sortNumbersCustom) {
			assertEquals(m, pq.poll())
		}
	}

	@Test
	fun removeSomeTest() {
		val pq = PriorityQueue.create<Int>()
		for (n in randomNumbers) {
			pq.add(n)
		}

		val sortNumbersCustom = sortNumbers.slice(sortNumbers.indices).toMutableList()
		randomNumbersSliced.forEach {
			pq.remove(it)
			sortNumbersCustom.remove(it)
		}

		for (m in sortNumbersCustom) {
			assertEquals(m, pq.poll())
		}
	}

	@Test
	fun emptyTest() {
		val pq = PriorityQueue.create<Int>()
		assertEquals(0, pq.size)
		assertTrue(pq.isEmpty())
	}

	@Test
	fun exceptionTest() {
		val pq = PriorityQueue.create<Int>()
		assertFails { pq.peek() }
	}

	@Test
	fun naiveTest1() {
		val pq = PriorityQueue.create<Int>()
		for (i in 10 downTo 1) {
			pq.add(i)
			assertEquals(i, pq.peek())
		}
		for (i in 1..10) {
			assertEquals(i, pq.poll())
		}
	}

	@Test
	fun naiveTest2() {
		val pq = PriorityQueue.create<Int>()
		for (i in 10 downTo 1) {
			pq.add(i)
			assertEquals(i, pq.peek())
		}
		for (i in -10..0) {
			pq.add(i)
		}
		for (i in -10..10) {
			assertEquals(i, pq.poll())
		}
	}
}